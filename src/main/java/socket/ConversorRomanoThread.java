package socket;

//import java.sql.Date;
//import java.util.Iterator;
//import java.util.Scanner;
import java.io.*;

import java.net.*;
import java.sql.Date;
import java.util.Scanner;

import datos.LogConversiones;
import datos.LogConversionesManager;
import romano.ConversorRomano;
//import py.com.dz.jtc.libreria.jdbc.datos.Libro;
import utils.LogUtil;

public class ConversorRomanoThread extends Thread {
    
	//Referencia al cliente que sera atendido
    private Socket cliente;
    
    public ConversorRomanoThread(Socket cliente) {
        this.cliente = cliente;
    }
    
    
    
    @Override
    public void run() {
    	
    	ServerSocket servidor = null;
    	LogUtil.INFO("Nueva peticion recibida...");
    	
        //TODO:process Pendiente de implementacion de logica que parsea peticion, procesa request y almacena resultado en la bd
    	
    	try {
			
			//cuando llegue a esta linea, es porque la anterior se ejecuto y avanzo
			//lo cual quiere decir que algun cliente se conecto a este servidor
			
			OutputStream clientOut = cliente.getOutputStream();
			PrintWriter pw = new PrintWriter(clientOut, true);
			
			//obtenemos el InputStream del cliente, para leer lo que nos dice
			InputStream clientIn = cliente.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
					
			//Leemos la primera linea del mensaje recibido
			String mensajeRecibido = br.readLine();
			//verificamos que viene en el mensaje
			
			 String nombre;
	    	 String nombreThread;
	    	 String ipCliente;
	    	 Date fechaHora;
	    	 Integer msgRequest;
	    	 String msgResponse;
	    	 LogConversiones lean;
	    	 InetAddress address;
	    	 address = cliente.getLocalAddress();
		
		if (mensajeRecibido != null) {
			if (mensajeRecibido.trim().equalsIgnoreCase("fin")) {
			//Nos enviaron la palabra fin, debemos parar el servidor rompiendo el while
			System.out.println("Gracias por jugar!");
			System.exit(0);
		} else if (mensajeRecibido.trim().startsWith("conv")) {
			//Nos enviaron la palabra conv debemos sacar el nro.
			
			String strnum = mensajeRecibido.substring(4);
			int numero = Integer.parseInt(strnum.trim());

			try {
				String valConvertido = ConversorRomano.decimal_a_romano(numero);
				pw.println("El valor convertido es: " + valConvertido);
				msgResponse = "Mensaje_Response ok";
			} catch (Exception e) {
				pw.println(e.getMessage());
				System.out.println("Tuvimos un error " + e.getMessage());
				msgResponse = e.getMessage();
			}
			ipCliente = address.getHostAddress().toString();
			nombre = this.nombreThread();
			
			//nombre = "Hilo " ;
			lean = new LogConversiones(nombre,ipCliente,numero,msgResponse);
			//se inserta en la tabla log_conversiones
			LogConversionesManager logcm = new LogConversionesManager();
			logcm.insertarNuevoRegistro(lean);
			pw.println("Procesado correctamente");
			
		}
	} else {
		pw.println("Mensaje recibido no valido.. reintente por favor!!");
	}
		//cerramos conexion con el cliente luego de cerrar los flujos
		clientIn.close();
		clientOut.close();
		cliente.close();
		} catch (IOException ie) {
		System.out.println("Error al procesar socket. " + ie.getMessage());
		}
	
    	LogUtil.INFO("Finalizando atencion de la peticion recibida...");
	
	}
	private String nombreThread() {
		LogConversionesManager contador = new LogConversionesManager();
		return "Cantidad Hilos " + contador.getCantidadRegistros();	    	
	}
    
}

