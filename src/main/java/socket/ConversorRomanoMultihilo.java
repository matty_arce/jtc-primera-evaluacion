package socket;

import java.net.*;
import java.io.*;
import romano.ConversorRomano;
import utils.LogUtil;

/**
 * Clase ConversorRomanoServer Multihilo
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ConversorRomanoMultihilo {

    public static void main(String[] args) {
        ServerSocket servidor = null;
        Socket cliente;
        ConversorRomanoThread thread;
        
        try {
            //Abre un servidor socket local en el puerto 6400
        	LogUtil.INFO("Iniciando socket server en puerto 6400");
            servidor = new ServerSocket(6400);
        } catch (IOException ie) {
            System.err.println("Error al abrir socket. " + ie.getMessage());
            System.exit(1);
        }
        
        LogUtil.INFO("Socket iniciado, se atienden peticiones..");
        //TODO:qlisto Pendiente de implementacion del procesador de peticiones
        try {
        	while(true){
        		cliente = servidor.accept();
        		thread = new ConversorRomanoThread(cliente);
        		thread.start();
        	}
        }catch (IOException ie){
        	System.out.println("Error al procesar el socket " + ie.getMessage());
        }
    }
       
}