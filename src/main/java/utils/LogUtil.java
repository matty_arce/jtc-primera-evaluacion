package utils;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Clase LogUtil.
 * <br/>Curso de Programacion Java
 * <br/>Implementa metodos utilitarios para manejo de log con Log4J
 * @author Derlis Zarate 
 */
public class LogUtil {
    
    /**
     * Atributos estaticos que almacenaran referencias a loggers de la aplicacion
     */
    private static Logger rootLogger;
    private static Logger appLogger;
        
    /**
     * Bloque static que inicializa las referencias a loggers luego de configurar el Log4J
     */
    static {
        try {            
            //El archivo de propiedades que configura el log4j puede estar fuera de la APP
            //por ejemplo en c:\temp\log4j.properties
            //PropertyConfigurator.configure("/temp/log4j.properties");
     
        	//O podria estar dentro de un paquete de la misma aplicacion, como en este ejemplo
            //entonces usamos el ClassLoader para cargar dicho archivo que esta entre las clases del sistema            
            PropertyConfigurator.configure(ClassLoader.getSystemClassLoader().getResourceAsStream("log4jConfig.properties"));
            
            rootLogger = Logger.getRootLogger(); 
            appLogger  = Logger.getLogger("socket");       
            
        } catch (Throwable ex) {
            System.err.println("Error al inicializar el logger. " + ex.getMessage());
            System.exit(1);
        }
    }
    
    /**
     * Retorna una referencia al Root Logger
     * @return root logger
     */
    public static Logger getRootLogger() {
        return rootLogger;
    }
    
    /**
     * Retorna una referencia al App Logger
     * @return app logger
     */
    public static Logger getAppLogger() {
        return appLogger;
    }
    
    /**
     * Metodo utilitario que escribe en el AppLogger en nivel INFO el mensaje recibido.
     * @param msg Mensaje a escribir en el log
     */
    public static void INFO(String msg) {
        appLogger.info(msg);
    }
    
    /**
     * Metodo utilitario que escribe en el AppLogger en nivel DEBUG el mensaje recibido.
     * @param msg Mensaje a escribir en el log
     */
    public static void DEBUG(String msg) {
        appLogger.debug(msg);
    }
    
    /**
     * Metodo utilitario que escribe en el AppLogger en nivel ERROR el mensaje recibido.
     * @param msg Mensaje a escribir en el log     
     */
    public static void ERROR(String msg) {
        appLogger.error(msg);
    }
    
    /**
     * Metodo utilitario que escribe en el AppLogger en nivel ERROR el mensaje recibido.
     * @param msg Mensaje a escribir en el log
     * @param t Objeto exception que dio el error
     */
    public static void ERROR(String msg, Throwable t) {
        appLogger.error(msg, t);
    }
    
} //Fin de clase LogUtil
