package datos;

import java.sql.Connection;
//import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import utils.JdbcUtil;
import utils.LogUtil;

/**
 * Clase LogConversionesManager. 
 * Implementa las operaciones sobre la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class LogConversionesManager {
    
    /**
     * Retorna la cantidad de registros en la tabla o -1 en caso de error
     * @return Nro de registros o -1 en caso de error
     */
    public int getCantidadRegistros() {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            int nro = -1;
            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select count(*) from LOG_CONVERSIONES");
                if (rs != null) {
                    if (rs.next()) {
                        nro = rs.getInt(1);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
                        
            return nro;
        } else {
            return -1;
        }        
    }

    /**
     * Metodo que retorna el listado completo de registros de la tabla en BD
     * @return Lista de LogConversiones
     */
    public List<LogConversiones> getAll() {
        List<LogConversiones> resp = new ArrayList<LogConversiones>();
        
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("select * from LOG_CONVERSIONES");
                if (rs != null) {
                    while (rs.next()) {//nombre_thread,ip_clientes,fecha_hora,msg_request,msg_response
                        LogConversiones lc = new LogConversiones(); //variante new LogConversiones(null, null, null, null, null);
                        /*
                         * TODO:listo Pendiente de realizar el mapeo Objeto Relacional
                         * Ejemplo
                         *   lc.setCampoXX(rs.getCampoXX(1));
                         */
                        //lc.setIdMensaje(rs.getInt(1));
                        lc.setNombreThread(rs.getString(2));
                        lc.setIpCliente(rs.getString(3));
                        lc.setFechaHora(rs.getDate(4));
                        lc.setMsgRequest(rs.getInt(5));
                        lc.setMsgResponse(rs.getString(6));
                        
                        resp.add(lc);
                    }
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
        
        return resp;
    }
    
    /**
     * Busca un registro por su ID
     * @param id ID del registro
     * @return Registro con ID o NULL si no existe
     */
    public LogConversiones buscarPorID(int id) {
        LogConversiones lc = null;
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {
            try {
                PreparedStatement stm = con.prepareStatement("select * from log_conversiones where id_mensaje = ?");
                stm.setInt(1, id);
                ResultSet rs = stm.executeQuery();
                if (rs != null) {
                    if (rs.next()) {
                        lc = new LogConversiones();
                        /*
                         * TODO:listo Pendiente de realizar el mapeo Objeto Relacional
                         * Ejemplo
                         *   lc.setCampoXX(rs.getCampoXX(1));
                         */
                        //lc.setIdMensaje(rs.getInt(1));
                        lc.setNombreThread(rs.getString(2));
                        lc.setIpCliente(rs.getString(3));
                        lc.setFechaHora(rs.getDate(4));
                        lc.setMsgRequest(rs.getInt(5));
                        lc.setMsgResponse(rs.getString(6));
                        
                    }
                }
                con.close();
            } catch (Exception ex) {
                LogUtil.ERROR("Error al ejecutar consulta", ex);
            }
                        
            return lc;
        } else {
            return null;
        }
    }
    
    /**
     * Inserta un nuevo registro en tabla 
     * @param LogConversiones
     */
    public void insertarNuevoRegistro(LogConversiones nuevoRegistro) {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
            	/*
            	 * TODO:listo Crear un preparedStatement para el insert y leer los parametros de cada campo
            	 * con las funciones get del objeto nuevoRegistro
            	 */
                PreparedStatement stm = con.prepareStatement("insert into log_conversiones (nombre_thread,ip_cliente,fecha_hora,msg_request,msg_response) values (?,?,?,?,?)");
                //stm.setInt(1, nuevoRegistro.getIdMensaje());
                stm.setString(1, nuevoRegistro.getNombreThread());
                stm.setString(2, nuevoRegistro.getIpCliente());
                stm.setTimestamp(3, new Timestamp (nuevoRegistro.getFechaHora().getTime()));
                stm.setInt(4, nuevoRegistro.getMsgRequest());
                stm.setString(5, nuevoRegistro.getMsgResponse());
          
                int rs = stm.executeUpdate();
                if (rs == 1) {
                    LogUtil.INFO("Registro creado exitosamente: " + nuevoRegistro);
                }
                //Conteo();
                con.commit();
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
    }/*
    int contador=0;
    public int Conteo(){
    	contador++;
    	return contador;
    }*/

    /**
     * Elimina el registro con ID recibido
     * @param codigo ID del registro a eliminar
     */
    public void eliminarRegistro(int id) {
        Connection con = JdbcUtil.getInstance().getConnection();
        if (con != null) {            
            try {
                PreparedStatement stm = con.prepareStatement("delete from log_conversiones where id_mensaje = ?");
                stm.setInt(1, id);
                
                int rs = stm.executeUpdate();
                if (rs == 1) {
                    LogUtil.INFO("Registro eliminado exitosamente");
                }
                con.close();
            } catch (Exception e) {
                LogUtil.ERROR("Error al ejecutar consulta", e);
            }
        }
    }
    
} //Fin de clase LogConversionesManager
