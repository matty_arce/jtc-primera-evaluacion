package datos;

import java.util.Date;
//import java.sql.*;

/**
 * Clase LogConversiones. Representa a 1 registro de la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class LogConversiones {
    
    //TODO:listo Crear estructura de la clase, constructor, getters y setters
	/*
	ID_MENSAJE integer not null generated always as identity (start with 1, increment by 1),
	NOMBRE_THREAD Varchar(50) not null, --Nombre del Thread que atendio la peticion
	IP_CLIENTE Varchar(15) not null, --Direccion IP del cliente que invoco al servidor
	FECHA_HORA Timestamp not null, --Fecha y hora de la peticion atendida
	MSG_REQUEST integer not null, --Num. decimal recibido como peticion a convertir
	MSG_RESPONSE Varchar(100), --Mensaje de respuesta generado para la peticion
								--Si se genero un error, aqui guardar msg del error
	constraint primary_key_log_conversiones primary key (id_mensaje)
	 * */
	//private Integer idMensaje;
	private String nombreThread;
	private String ipCliente;
	private Date fechaHora;
	private Integer msgRequest;
	private String msgResponse;
	
	public LogConversiones(String nombreThread, String ipCliente, /*Date fechaHora,*/ Integer msgRequest, String msgResponse){
		
		this.setNombreThread(nombreThread);
		this.setIpCliente(ipCliente);
		this.fechaHora = new Date();
		this.setMsgRequest(msgRequest);
		this.setMsgResponse(msgResponse);
		
	}
		
	public LogConversiones() {
		
	}
/*
	public Integer getIdMensaje() {
		return idMensaje;
	}
	public void setIdMensaje(Integer idMensaje) {
		this.idMensaje = idMensaje;
	}*/
	public String getNombreThread() {
		return nombreThread;
	}
	public void setNombreThread(String nombreThread) {
		this.nombreThread = nombreThread;
	}
	public String getIpCliente() {
		return ipCliente;
	}
	public void setIpCliente(String ipCliente) {
		this.ipCliente = ipCliente;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	public Integer getMsgRequest() {
		return msgRequest;
	}
	public void setMsgRequest(Integer msgRequest) {
		this.msgRequest = msgRequest;
	}
	public String getMsgResponse() {
		return msgResponse;
	}
	public void setMsgResponse(String msgResponse) {
		this.msgResponse = msgResponse;
	}
	
	
	
    
} //Fin de clase LogConversiones
